#include <iostream>
#include <SFML/Graphics.hpp>
#include "config.h"


using std::cout;
using std::endl;
using std::string;

int main(int argc, char *argv[])
{

    string title = "Super jeu";
    cout << "Version " << QUICKGAME_VERSION_MAJOR << "." << QUICKGAME_VERSION_MINOR << endl;

    sf::RenderWindow window(sf::VideoMode(800, 600), title);

    sf::Texture background_texture;
    if (!background_texture.loadFromFile(std::string(QUICKGAME_ASSETS) + "/background.png"))
    {
        throw std::runtime_error("Could not find texture");
    }
    sf::Sprite sprite;
    sprite.setTexture(background_texture);

    sf::Texture character_texture;
    if (!character_texture.loadFromFile(std::string(QUICKGAME_ASSETS) + "/character.png", sf::IntRect(0, 0, 96, 96)))
    {
        throw std::runtime_error("Could not find texture");
    }
    sf::Sprite character_sprite;
    character_sprite.setTexture(character_texture);

    while (window.isOpen())
    {
        sf::Event Event{};
        while (window.pollEvent(Event))
        {
            if (Event.type == sf::Event::Closed)
            {
                window.close();
            }
        }
        window.clear(sf::Color::Black);
        window.draw(sprite);
        window.draw(character_sprite);
        window.display();
    }
    return 0;
}